import src/ast/ast;

export {

    // Try to evaluate expr. 
    evaluateExpression(expr : Expression, variableValues : Tree<string, double>) -> Maybe<double>;

    // Same as evaluateExpression but does not replace variables with values and does not report arithmetic errors
    // If expr contains no variables and no arithmetic errors occur, the result is a number expr evaluates to
    evaluateExpressionSilently(expr : Expression) -> Maybe<double>;
}

evaluateExpression(expr : Expression, variableValues : Tree<string, double>) -> Maybe<double> {
    applyOp = \ left : Expression, right : Expression, op : (double, double) -> Maybe<double> -> {
        switch (evaluateExpression(left, variableValues) : Maybe<double>) {
            Some(leftValue): switch (evaluateExpression(right, variableValues) : Maybe<double>) {
                Some(rightValue): op(leftValue, rightValue);
                None(): None();
            }
            None(): None();
        }
    }

	switch (expr : Expression) {
		Add(left, right): applyOp(left, right, \ x, y -> Some(x + y));
		Subtract(left, right): applyOp(left, right, \ x, y -> Some(x - y));
        Negative(value): applyOp(Number(0.0), value, \ x, y -> Some(x - y));
		Multiply(left, right): applyOp(left, right, \ x, y -> Some(x * y));
		Divide(left, right): applyOp(left, right, \ x, y -> {
            if (y != 0.0) {
                Some(x / y);
            } else {
                println("Cannot evaluate \"" + expressionToString(expr) + "\": division by zero (" + d2s(x) + " / 0)");
                None();
            }
        });
		Power(left, right): applyOp(left, right, \ x, y -> {
            if (x > 0.0) {
                Some(exp(y * log(x)));
            } else if (isInt(y) && x != 0.0) {
                result : double = exp(y * log(abs(x)));
                Some(if (x < 0.0 && !even(round(y))) -result else result);
            } else if (x == 0.0 && y != 0.0) {
                Some(0.0);
            } else {
                println(
                    if (x == 0.0 && y == 0.0) {
                        "Cannot evaluate \"" + d2s(x) + "**" + d2s(y) + "\""
                    } else {
                        "Cannot evaluate \"" + d2s(x) + "**" + d2s(y) 
                            + "\": exponent is not integer (although it may seem so)"
                    }
                );
                None();
            }
        });
		Log(expression): {
            switch (evaluateExpression(expression, variableValues) : Maybe<double>) {
                Some(value): {
                    if (value > 0.0) {
                        Some(log(value));
                    } else {
                        println("Cannot apply log to negative value or 0");
                        None();
                    }
                }
                None(): None();
            }
        }
		Number(value): Some(value);
		Variable(name): {
            lookupResult : Maybe<double> = lookupTree(variableValues, name);
            if (isNone(lookupResult)) {
                println("No value provided for variable \"" + name + "\"");
            } else {}
            lookupResult;
        }
	}
}

isInt(d : double) -> bool {
    i2d(round(d)) == d
}

evaluateExpressionSilently(expr : Expression) -> Maybe<double> {
    applyOp = \ left : Expression, right : Expression, op : (double, double) -> Maybe<double> -> {
        switch (evaluateExpressionSilently(left) : Maybe<double>) {
            Some(leftValue): switch (evaluateExpressionSilently(right) : Maybe<double>) {
                Some(rightValue): op(leftValue, rightValue);
                None(): None();
            }
            None(): None();
        }
    }

	switch (expr : Expression) {
		Add(left, right): applyOp(left, right, \ x, y -> Some(x + y));
		Subtract(left, right): applyOp(left, right, \ x, y -> Some(x - y));
        Negative(value): applyOp(Number(0.0), value, \ x, y -> Some(x - y));
		Multiply(left, right): applyOp(left, right, \ x, y -> Some(x * y));
		Divide(left, right): applyOp(left, right, \ x, y -> {
            if (y != 0.0) { Some(x / y); } else { None(); }
        });
		Power(left, right): applyOp(left, right, \ x, y -> {
            if (x > 0.0) {
                Some(exp(y * log(x)));
            } else if (isInt(y) && x != 0.0) {
                result : double = exp(y * log(abs(x)));
                Some(if (x < 0.0 && !even(round(y))) -result else result);
            } else { 
                None();
            }
        });
		Log(expression): {
            switch (evaluateExpressionSilently(expression) : Maybe<double>) {
                Some(value): if (value > 0.0) { Some(log(value)); } else { None(); }
                None(): None();
            }
        }
		Number(value): Some(value);
		Variable(name): None();
	}
}
