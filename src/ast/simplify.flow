import src/ast/ast;
import src/ast/evaluate;

export {
    simplify(expr : Expression) -> Expression;
}

simplify(expr : Expression) -> Expression {
    simplified : Expression = simplifyRaw(expr);
    switch (evaluateExpressionSilently(simplified) : Maybe<double>) {
        Some(value): Number(value);
        None(): simplified;
    }
}

simplifyRaw(expr : Expression) -> Expression {
    switch (evaluateExpressionSilently(expr) : Maybe<double>) {
        Some(value): Number(value);
        None(): switch (expr : Expression) {
            Add(left, right): {
                leftSimple : Expression = simplify(left);
                rightSimple : Expression = simplify(right);
                if (leftSimple == Number(0.0)) { rightSimple; }
                else if (rightSimple == Number(0.0)) { leftSimple; }
                else { Add(leftSimple, rightSimple); }
            }
            Subtract(left, right): {
                if (left == right) {
                    Number(0.0);
                } else {
                    leftSimple : Expression = simplify(left);
                    rightSimple : Expression = simplify(right);
                    if (leftSimple == Number(0.0)) { Negative(rightSimple); }
                    else if (rightSimple == Number(0.0)) { leftSimple; }
                    else { Subtract(leftSimple, rightSimple); }
                }
            }
            Negative(expression): {
                switch (expression : Expression) {
                    Number(value): Number(-value);
                    Negative(other): simplify(other);
                    default: Negative(simplify(expression));
                }
            }
            Multiply(left, right): {
                leftSimple : Expression = simplify(left);
                rightSimple : Expression = simplify(right);
                if (leftSimple == Number(0.0) || rightSimple == Number(0.0)) { Number(0.0); }
                else if (rightSimple == Number(1.0)) { leftSimple; }
                else if (leftSimple == Number(1.0)) { rightSimple; }
                else { Multiply(leftSimple, rightSimple); }
            }
            Divide(left, right): {
                leftSimple : Expression = simplify(left);
                rightSimple : Expression = simplify(right);
                if (rightSimple == Number(0.0)) { Divide(leftSimple, rightSimple); }
                else if (rightSimple == leftSimple) { Number(1.0); }
                else if (leftSimple == Number(0.0)) { Number(0.0); }
                else if (rightSimple == Number(1.0)) { leftSimple; }
                else { Divide(leftSimple, rightSimple); }
            }
            Power(left, right): {
                leftSimple : Expression = simplify(left);
                rightSimple : Expression = simplify(right);
                if (rightSimple == Number(0.0) && leftSimple != Number(0.0) && !isVariable(leftSimple)) { Number(1.0); }
                else if (leftSimple == Number(0.0) && rightSimple != Number(0.0) && !isVariable(rightSimple)) { Number(0.0); }
                else if (rightSimple == Number(1.0)) { leftSimple; }
                else { Power(leftSimple, rightSimple); }
            }
            Log(expression): {
                expressionSimplified : Expression = simplify(expression);
                if (expressionSimplified == Number(1.0)) { Number(0.0); } 
                else { Log(expressionSimplified); };
            }
            Number(value): expr;
            Variable(name): expr;
        }
    }
}

isVariable(expr : Expression) -> bool {
    switch (expr : Expression) {
        Variable(unused): true;
        default: false;
    }
}
